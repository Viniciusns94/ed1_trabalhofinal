/*-----------------------------------------------------------
* UNIFAL : Universidade Federal de Alfenas.
* Trabalho..: Casamento de padrão
* Disciplina: Estrutura de Dados I
* Professor.: Luiz Eduardo da Silva
* Aluno(s)..: Rodrigo de Olveira Figueiredo
			  Victor Rodrigues Periz
			  Guilherme Vieira
			  Vinicius Nogueira
* Data......: 22/06/2016
*------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define ALPHABET_SIZE 400

typedef struct no {
	char info;
	struct no *prox;
} *ptno;

typedef struct strno {
	int tamanho;
	ptno inicio;
} *string;

string criaString (char *s) {
	ptno p, q = NULL;
	string L = (string) malloc(sizeof (struct strno));
	L->inicio = NULL;
	int i = 0;
	while (s[i]) {
		p = (ptno) malloc(sizeof (struct no));
		p->info = s[i];
		p->prox = NULL;
		if (!(L->inicio)) L->inicio = p;
		if (q) q->prox = p;
		q = p;
		i++;
	}
	L->tamanho = i;
	return L;
}

void escreveString (string s) {
	ptno p = s->inicio;
	while (p) {
		putchar(p->info);
		p = p->prox;
	}
}

int tamanho (string s){
	return s->tamanho;
}

string carregaTexto (char *nomeArquivo) {
	
	string L;
    FILE *arquivo;
    arquivo = fopen(nomeArquivo, "r");
    int cont=0, i=0;
    char c;
    // Conta o numero de caracteres do Texto
    if(arquivo != NULL){
        while( (c=fgetc(arquivo))!= EOF ){
            cont++;
        }
    }
    
	fclose(arquivo);
    char ch[cont];
    
    arquivo = fopen(nomeArquivo, "r");
    // Atribui caracteres do texto no vetor
    if(arquivo != NULL){
        while( (c=fgetc(arquivo))!= EOF ){     	
        	ch[i] = c;
			i++;
		}
    }
    
    ch[cont] = '\0';
   	// Cria String 
    L = criaString(ch);
    
	fclose(arquivo);
	
    return L;
}

void BMH (string s, char *padrao, int sensivel){
	
	int i, j, k;
	int skip[ALPHABET_SIZE];
	int cont, linhaPalavra, linhaAtual, indicador, n = s->tamanho, m = strlen(padrao);	
	char novoPadrao[m];
	// Quantidade de caracteres do texto
	
	ptno p = s->inicio;

	if(sensivel == 0){
		for(k=0; k<m; k++)
			novoPadrao[k] = tolower(padrao[k]);
		
		padrao = novoPadrao;
		padrao[k] = '\0';
	}
			
	for(k=0; k<ALPHABET_SIZE; k++) skip[k]= m;
	for(k=0; k<m-1; k++) skip[padrao[k]]= m-k-1;
		
			// Caso seja sensivel tambem tem que colocar o valor do skip para minusculo se maisculo, e maisculo se minusculo
			if(sensivel == 0){				
				for(k=0; k<m-1; k++){
					skip[toupper(padrao[k])] = skip[padrao[k]];	
				}
			}	
				
			for(i=m-1; i<n; i+=skip[p->info]){
				
				cont = 0;
				
				for(j=m-1, k=i; j>=0; j--){					
									
					p = s->inicio;
					indicador = 0;
					while (indicador != k){					
						p = p->prox;
						indicador++;
					}
					
					if(sensivel == 1){
						if(p->info == padrao[j]){
							k--;
							cont++;
						}
					}else{
						if(p->info == padrao[j] || p->info == toupper(padrao[j])){
							k--;
							cont++;
						}
					}
				}	
				
				if (cont == strlen(padrao)){
					
					// Encontra a linha que a palavra esta
					p = s->inicio;
    				linhaPalavra = 1;
    				j = 0;
    				while(j != i){
        				if(p->info == '\n'){
            				linhaPalavra++;
        				}
        				p = p->prox;
        				j++;
    				}
    				// printa numero da linha
    				if(linhaPalavra<10){
    					printf(" %d: ", linhaPalavra);
    				}else{
    					printf("%d: ", linhaPalavra);
    				}
    				//printa a linha
    				p = s->inicio;
    				linhaAtual = 1;
    				while (p) {
    					if(p->info == '\n'){
            				linhaAtual++;
        				}
        				if(linhaAtual == linhaPalavra){ 
							if(p->info != '\n'){
								printf("%c", p->info);
							}    
						}						
						p = p->prox;
					}
										
					// print o começo da de baixo
					printf("\n    ");					
					// printa o fim da de baixo
					j = 0;
					p = s->inicio;
					linhaAtual = 1;
					int posicaoPalavra = i - strlen(padrao)+1;     				
					while (p) {						
						if(p->info == '\n'){
            				linhaAtual++;
        				}
        				if(linhaAtual == linhaPalavra){
        					if(p->info != '\n'){
								if(j != posicaoPalavra){
									printf(" ");
								}else{
									printf("^");
								}
							}  
						}
						p = p->prox;
						j++;
					} 
					printf("\n");					
				} 				
								
				// Vai ate a posicao i e da o skip de acordo com o valor																	
				p = s->inicio;
				indicador = 0;
				while (indicador != i) {					
					p = p->prox;
					indicador++;
				}
			}						
}

int main (){
	string texto = carregaTexto("teste.txt");	
	BMH(texto, "ciclo", 0);	
}
