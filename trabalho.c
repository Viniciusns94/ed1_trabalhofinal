#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define ALPHABET_SIZE 400

typedef struct no {
	char info;
	struct no *prox;
} *ptno;

typedef struct strno {
	int tamanho;
	ptno inicio;
} *string;

string criaString (char *s) {
	ptno p, q = NULL;
	string L = (string) malloc(sizeof (struct strno));
	L->inicio = NULL;
	int i = 0;
	while (s[i]) {
		p = (ptno) malloc(sizeof (struct no));
		p->info = s[i];
		p->prox = NULL;
		if (!(L->inicio)) L->inicio = p;
		if (q) q->prox = p;
		q = p;
		i++;
	}
	L->tamanho = i;
	return L;
}

void escreveString (string s) {
	ptno p = s->inicio;
	while (p) {
		putchar(p->info);
		p = p->prox;
	}
}

int tamanho (string s){
	return s->tamanho;
}

string carregaTexto (char *nomeArquivo) {	//que retorna um string com todo conteudo do arquivo texto, cujo nome é passado como parametro
	
	string L;
    FILE *arquivo;
    arquivo = fopen(nomeArquivo, "r");
    int cont=0, i=0;
    char c;
    
    if(arquivo != NULL){
        while( (c=fgetc(arquivo))!= EOF ){
            cont++;
        }
    }
    
	fclose(arquivo);
    char ch[cont];
    
    arquivo = fopen(nomeArquivo, "r");
    
    if(arquivo != NULL){
        while( (c=fgetc(arquivo))!= EOF ){     	
        	ch[i] = c;
			i++;
		}
    }
    
    ch[cont] = '\0';    
    L = criaString(ch);    
	fclose(arquivo);	
    return L;
}
/*recebe string e um vetor de caracteres com o padrao a ser usado e escreve a linha onde o padrao foi
  encontrado e uma outra com o simbolo ^ marcando a palavra encontrada
  Sensivel: ve se difere maiscula de minuscula - se 1 difere*/
	
void BMH (string s, char *padrao, int sensivel){
	int i, j, k;
	int skip[ALPHABET_SIZE];
	
	int cont, linha, linhaAtual, indicador , n = s->tamanho, m = strlen(padrao);	

	ptno p = s->inicio;
			
	for(k=0; k<ALPHABET_SIZE; k++) 
		skip[k]= m; // todos as letras do alfabeto inicializam como 5
	
	for(k=0; k<m-1; k++) 
		skip[padrao[k]]= m-k-1; // cada letra do padrao de 0 < m-1 recebe m-1 a 1			
			
	if(!sensivel){
		for(k=0; k<m-1; k++){// colocando os padroes maisculo e minusculo com os mesmo valores
			if(isupper(padrao[k]))
				skip[tolower(padrao[k])] = skip[padrao[k]];
        	else 
        		skip[toupper(padrao[k])] = skip[padrao[k]];        			
		}
	}					
	
	for(i=m-1; i<n; i+=skip[p->info]) { //i vai do tamanho do padrao-1 ao fim do texto
		cont = 0;

		for(j=m-1, k=i; j>=0; j--){					
			p = s->inicio;
			indicador = 0;
			while (indicador != k) {					
				p = p->prox;
				indicador++;
			}
					
			if(sensivel == 1){
				if(p->info == padrao[j]){
					k--;
					cont++;
				}
			}
			else{
				if(toupper(p->info) == padrao[j] || tolower(p->info) == padrao[j]){
					k--;
					cont++;
				}
			}															
		}					
		if (cont == strlen(padrao)){ // padrao achado em i 
			p = s->inicio;
    		linha = 1;
    		j = 0;
    		while(j != i){
        		if(p->info == '\n'){
            		linha++;
        		}
        		p = p->prox;
        		j++;
    		}
    		// printa numero da linha
    		if(linha<10)
    			printf(" %d: ", linha);
    		else
    			printf("%d: ", linha);
    			
    		p = s->inicio;
    		linhaAtual = 1;
    		while (p) {
    			if(p->info == '\n')
            		linhaAtual++;
       
        		if(linhaAtual == linha){ 
					if(p->info != '\n')
						printf("%c", p->info);
				}						
				p = p->prox;
			}					
					
			printf("\n");
			printf(" ");
			printf(" ");
			printf(" ");					
			printf(" ");
					
			if(linha == 1)
				j = 0;
			else
				j = 1;
			
			p = s->inicio;
			linhaAtual = 1;
			int posicao = i - strlen(padrao)+1;     				
			while (p) {						
				if(p->info == '\n')
            	linhaAtual++;
        				
        		if(linhaAtual == linha){
					if(j != posicao)
						printf(" ");
					else
						printf("^");
				}
				p = p->prox;
				j++;
			} 
			printf("\n");					
		} 				
																					
		p = s->inicio;
		indicador = 0;
		while (indicador != i) {					
			p = p->prox;
			indicador++;
		}
	}						
}

int main (){	
	string texto = carregaTexto("teste.txt");	
	BMH(texto, "ciclo", 0);	
}